from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Conv1D, MaxPooling1D, Embedding, Conv2D, MaxPooling2D, Input, Bidirectional, concatenate
from keras.models import Sequential, Model, load_model
from keras.layers.recurrent import LSTM
from keras.layers.core import Dense, Dropout, Reshape, Permute
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
from keras import regularizers
import keras.backend as K

import io
import operator
import progressbar
import numpy as np

from util import load_conll, eval_result, dev_and_test_comb, gen_data, pad_data, pad_label, pad_word_input, load_index, data_to_seq
from config import config

texts, labels = load_conll(config.train_path_transfer, config.labels_index)
val_texts, val_labels = load_conll(config.dev_path_transfer, config.labels_index)
#texts, labels = load_conll('keras_data/1.txt')
test_texts, test_labels = load_conll(config.test_path_transfer, config.labels_index)

index_char = load_index(config.char_index)

MAX_WORD_LENGTH = config.word_length
wl = MAX_WORD_LENGTH

train_char,sl,wl = gen_data(texts,0,wl,index_char)
val_char,sl,wl = gen_data(val_texts,sl,wl,index_char)
test_char,sl,wl = gen_data(test_texts,sl,wl,index_char)

MAX_SEQUENCE_LENGTH = sl

if MAX_SEQUENCE_LENGTH % 2 == 1:
   MAX_SEQUENCE_LENGTH += 1
print(MAX_WORD_LENGTH)
print(MAX_SEQUENCE_LENGTH)

train_data_char = pad_data(train_char,MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH)
val_data_char = pad_data(val_char,MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH)
test_data_char = pad_data(test_char,MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH)

print(np.shape(train_char))

labels = pad_label(labels, MAX_SEQUENCE_LENGTH)
val_labels = pad_label(val_labels, MAX_SEQUENCE_LENGTH)
test_labels = pad_label(test_labels, MAX_SEQUENCE_LENGTH)

index_word = load_index(config.word_index)

train_data_word = data_to_seq(texts, index_word)
val_data_word = data_to_seq(val_texts, index_word)
test_data_word = data_to_seq(test_texts, index_word)

#pad word input
train_data_word = pad_word_input(train_data_word, MAX_SEQUENCE_LENGTH)
val_data_word = pad_word_input(val_data_word, MAX_SEQUENCE_LENGTH)
test_data_word = pad_word_input(test_data_word, MAX_SEQUENCE_LENGTH)

model = load_model(config.source_model)

print("Testing")
acc, p, r, f1 = dev_and_test_comb(test_data_word, test_data_char, test_labels, model, config, test = True)
