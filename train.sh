#!/bin/bash
#SBATCH --get-user-env
#SBATCH --job-name="che313"
#SBATCH --time=05:00:00
#SBATCH --nodes=1
#SBATCH --mem=50GB
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
echo "loading python tf keras"
module load python/3.6.1
module load tensorflow/1.2.1-py35-gpu
module load keras/2.0.8-py36
echo "loaded"

python combine.py
