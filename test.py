from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Conv1D, MaxPooling1D, Embedding, Conv2D, MaxPooling2D, Input, Bidirectional, concatenate
from keras.models import Sequential, Model, load_model
from keras.layers.recurrent import LSTM
from keras.layers.core import Dense, Dropout, Reshape, Permute
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
from keras import regularizers
import keras.backend as K

import io
import operator
import progressbar
import numpy as np

from util import load_conll, eval_result, dev_and_test_comb, gen_data, pad_data, pad_label, pad_word_input, load_index, data_to_seq
from config import config

model_old = load_model(config.source_model)
for layer in model_old.layers:
    print(layer.name)
print(model_old.layers[2].get_weights())
#print(model_old.layers[3].get_weights())
