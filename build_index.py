from keras.preprocessing.text import Tokenizer
from util import save_index, load_conll
from config import config

mpl_texts, labels = load_conll('data_0120/60_mpl_train.txt')
mpl_val_texts, val_labels = load_conll('data_0120/20_mpl_dev.txt')
#texts, labels = load_conll('data_0120/1.txt')
mpl_test_texts, test_labels = load_conll('data_0120/20_mpl_test.txt')

pd_texts, labels = load_conll('data_0120/60_pd_train.txt')
pd_val_texts, val_labels = load_conll('data_0120/20_pd_dev.txt')
#texts, labels = load_conll('data_0120/1.txt')
pd_test_texts, test_labels = load_conll('data_0120/20_pd_test.txt')

np_texts, labels = load_conll('data_0120/60_np_train.txt')
np_val_texts, val_labels = load_conll('data_0120/20_np_dev.txt')
#texts, labels = load_conll('data_0120/1.txt')
np_test_texts, test_labels = load_conll('data_0120/20_np_test.txt')
'''
np_texts, labels = load_conll('data_0120/np_0921/60_np_train.txt')
np_val_texts, val_labels = load_conll('data_0120/np_0921/20_np_dev.txt')
#texts, labels = load_conll('data_0120/1.txt')
np_test_texts, test_labels = load_conll('data_0120/np_0921/20_np_test.txt')
'''
texts_all = mpl_texts + mpl_val_texts + mpl_test_texts + pd_texts + pd_val_texts + pd_test_texts + np_texts + np_val_texts + np_test_texts

tokenizer = Tokenizer(num_words=config.MAX_NB_WORDS, split=" ", lower=False, char_level=True, filters='')
tokenizer.fit_on_texts(texts_all)
index = tokenizer.word_index
save_index(index, 'char_index')
