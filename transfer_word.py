from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Conv1D, MaxPooling1D, Embedding, Conv2D, MaxPooling2D, Input, Bidirectional
from keras.models import Sequential, Model, load_model
from keras.layers.recurrent import LSTM
from keras.layers.core import Dense, Dropout, Reshape, Permute
from keras.layers.wrappers import TimeDistributed
from keras import regularizers
import keras.backend as K

import io
import operator
import progressbar
import numpy as np

from util import load_conll, eval_result, dev_and_test_word, gen_data, pad_data, pad_label, pad_word_input, load_index, data_to_seq
from config import config

#np.random.seed(7)

texts, labels = load_conll(config.train_path_transfer)
val_texts, val_labels = load_conll(config.dev_path_transfer)
#texts, labels = load_conll('keras_data/1.txt')
test_texts, test_labels = load_conll(config.test_path_transfer)
'''
texts_all = texts + val_texts + test_texts
tokenizer = Tokenizer(num_words=config.MAX_NB_WORDS, split=" ", lower=False, char_level=False, filters='')
tokenizer.fit_on_texts(texts_all)

data = tokenizer.texts_to_sequences(texts)
val_data = tokenizer.texts_to_sequences(val_texts)
test_data = tokenizer.texts_to_sequences(test_texts)

word_index = tokenizer.word_index
'''

word_index = load_index('word_index')

data = data_to_seq(texts, word_index)
val_data = data_to_seq(val_texts, word_index)
test_data = data_to_seq(test_texts, word_index)

embeddings_index = {}
with io.open(config.word_embedding_path, 'r',encoding='utf8') as f:
    for line in f.readlines():
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()

print('Found %s word vectors.' % len(embeddings_index))

#num_words = min(config.MAX_NB_WORDS, len(word_index))
num_words = len(word_index)
embedding_matrix = np.zeros((num_words + 1, config.dim_word_emb))
for word, i in word_index.items():
    if i >= config.MAX_NB_WORDS:
        continue
    embedding_vector = embeddings_index.get(word)
    #if i == 1:
        #print(embedding_vector)
    if embedding_vector is not None:
        # words not found in embedding index will be all-zeros.
        embedding_matrix[i] = embedding_vector

model = Sequential() # or Graph or whatever
model.add(Embedding(output_dim=config.dim_word_emb, input_dim=num_words + 1, mask_zero=True, weights=[embedding_matrix], trainable=False, name = 'sor_emb'))
#model.add(Conv1D(128, 5, activation='relu'))
#model.add(MaxPooling1D(5))
model.add(Dense(100, activation='relu', name = 'share_linaer'))
if config.bilstm:
    model.add(Bidirectional(LSTM(config.lstm_dim, return_sequences=True, name = 'sor_lstm')) )
else:
    model.add(LSTM(config.lstm_dim, return_sequences=True, name = 'sor_lstm')) 
model.add(Dropout(0.5, name = 'tar_linear'))
model.add(TimeDistributed(Dense(config.index_dim, activation='softmax', activity_regularizer=regularizers.l1(0.01), name = 'tar_clsfier')))

model.load_weights(config.source_weight, by_name=True)

model.compile(loss='categorical_crossentropy',
              optimizer='adam')

best_f1 = 0
no_improve = 0
result = open(config.log,'w')

for i in range(config.n_epochs):
    print("Epoch {}".format(i))
    print("continue Training")

    train_pred_label = []
    avgLoss = 0
    bar = progressbar.ProgressBar(max_value=len(data))

    for n_batch, sent in bar(enumerate(data)):
        #print(n_batch)
        label = labels[n_batch]
        #print(label)
        label = np.array(label)
        label = label[np.newaxis,:]
        #print(label)
        label = np.eye(config.index_dim)[label]
        #print(label)
        sent = data[n_batch]
        #print(sent)
        sent = np.array([sent])
        #print(sent.shape)
        #print(label.shape)
        #if sent.shape[1] > 1:
        loss = model.train_on_batch(sent, label)
            #for l in loss:
            #    print(l)
        avgLoss += loss
            #print(len(loss))
            #avgLoss = avgLoss/len(loss)
        #the reason why get two return of loss is accurcary

        pred = model.predict_on_batch(sent)
        pred = np.argmax(pred,-1)[0]
        train_pred_label.append(pred)
        #print(i)
        #count += 1
        
    avgLoss = avgLoss/len(data)
    print(avgLoss)
    #print(train_pred_label)

    predword_train = [ list(map(lambda x: config.l_in[x], y)) for y in train_pred_label]
    #print(predword_train)

    eval_result(labels, predword_train)
    
    print("Validating")
    acc, p, r, f1 = dev_and_test_word(val_data, val_labels, model, test = False)
    result.write('train lr' + str(float(K.get_value(model.optimizer.lr))) + ' decay: ' + str(float(K.get_value(model.optimizer.decay))) + "\nValidating " + ' acc: ' + str(acc) + ' p: ' + str(p) + ' r: ' + str(r) + ' f1: ' + str(f1)+'\n')
    if f1 >= best_f1:
        best_f1 = f1
        no_improve = 0
        model.save_weights(config.model_weight_path)
        model.save(config.model_path)
        print("new best score!")
        result.write("new best score!\n")
    else:
        no_improve += 1
        print("no_improve: " + str(no_improve))
        if no_improve >= config.train_limit:
            print("early stopping")
            result.write("early stopping\n")
            break

    print("Testing")
    acc, p, r, f1 = dev_and_test_char(test_data, test_labels, model, test = True)
    result.write("Testing " + ' acc: ' + str(acc) + ' p: ' + str(p) + ' r: ' + str(r) + ' f1: ' + str(f1)+'\n')
    result.write('\n')

