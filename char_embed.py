#from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Conv1D, MaxPooling1D, Embedding, Conv2D, MaxPooling2D, Input, Bidirectional
from keras.models import Sequential, Model
from keras.layers.recurrent import LSTM
from keras.layers.core import Dense, Dropout, Reshape, Permute
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
from keras import regularizers
import keras.backend as K

import io
import operator
import progressbar
import numpy as np

from util import load_conll, eval_result, dev_and_test_char, gen_data, pad_data, pad_label, pad_word_input, load_index
from config import config

#np.random.seed(7)
print(config.train_path)
texts, labels = load_conll(config.train_path)
val_texts, val_labels = load_conll(config.dev_path)
#texts, labels = load_conll('keras_data/1.txt')
test_texts, test_labels = load_conll(config.test_path)

#texts_all = texts + val_texts + test_texts
#tokenizer = Tokenizer(num_words=config.MAX_NB_WORDS, split=" ", lower=False, char_level=True, filters='')
#tokenizer.fit_on_texts(texts_all)

index = load_index('char_index')
#index_to_print = sorted(index.items(), key=operator.itemgetter(1))
#print(index_to_print)
#print(len(index))

MAX_WORD_LENGTH = config.word_length
wl = MAX_WORD_LENGTH

train,sl,wl = gen_data(texts,0,wl,index)
val,sl,wl = gen_data(val_texts,sl,wl,index)
test,sl,wl = gen_data(test_texts,sl,wl,index)

MAX_SEQUENCE_LENGTH = sl


if MAX_SEQUENCE_LENGTH % 2 == 1:
   MAX_SEQUENCE_LENGTH += 1
print(MAX_WORD_LENGTH)
print(MAX_SEQUENCE_LENGTH)

train = pad_data(train,MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH)
val_data = pad_data(val,MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH)
test_data = pad_data(test,MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH)

labels = pad_label(labels, MAX_SEQUENCE_LENGTH)
val_labels = pad_label(val_labels, MAX_SEQUENCE_LENGTH)
test_labels = pad_label(test_labels, MAX_SEQUENCE_LENGTH)

num_chars = len(index)

model = Sequential() # or Graph or whatever
#embedding shape (None/word num in a sent, max_char_num_in_word, char_emb_dim)
model.add(Embedding(input_dim=num_chars, output_dim=config.dim_char_emb, input_length=MAX_SEQUENCE_LENGTH*MAX_WORD_LENGTH, name = 'sor_char_embed'))
model.add(Reshape((MAX_SEQUENCE_LENGTH,MAX_WORD_LENGTH, config.dim_char_emb), name = 'sor_reshape_1'))

model.add(Permute((3,1,2), name = 'sor_per_1'))
model.add(Conv2D(config.windows_size, (1, 2), padding='same', name = 'sor_conv'))
# 5 -> 125, 3 -> 75
model.add(Permute((2,1,3), name = 'sor_per_2'))
model.add(MaxPooling2D((2, 2), name = 'sor_max'))
model.summary()
model.add(Reshape((MAX_SEQUENCE_LENGTH, config.dim_reshape), name = 'sor_reshape_2'))
if config.bilstm:
    model.add(Bidirectional(LSTM(config.lstm_dim, return_sequences=True, name = 'sor_lstm')) )
else:
    model.add(LSTM(config.lstm_dim, return_sequences=True, name = 'sor_lstm')) 
model.add(TimeDistributed(Dense(config.index_dim, activation='softmax', activity_regularizer=regularizers.l1(0.01), name = 'sor_clsfier')))

ada = Adam(lr=config.lr)

model.summary()
#model.compile(loss='categorical_crossentropy', optimizer=ada)
model.compile(loss='categorical_crossentropy',
              optimizer='adam')

best_f1 = 0
no_improve = 0
result = open(config.log,'w')

for i in range(config.n_epochs):
    print("Epoch {}".format(i))
    print("Training")

    train_pred_label = []    
    avgLoss = 0   
    bar = progressbar.ProgressBar(max_value=len(train))

    for n_batch, sent in bar(enumerate(train)):
        #print(n_batch)
        label = labels[n_batch]
        #print(label)
        label = np.array(label)
        label = label[np.newaxis,:]
        #print(label)
        label = np.eye(config.index_dim)[label]
        #print(label)
        #print(sent)
        sent = np.array([sent])
        #print(sent)
        #print(label.shape)
        loss = model.train_on_batch(sent, label)
            #for l in loss:
            #    print(l)
        avgLoss += loss
            #print(len(loss))
            #avgLoss = avgLoss/len(loss)
        #the reason why get two return of loss is accurcary
        pred = model.predict_on_batch(sent)
        pred = np.argmax(pred,-1)[0]
        train_pred_label.append(pred)
        #print(i)
        #count += 1
        
    avgLoss = avgLoss/len(train)
    print(avgLoss)
    #print(train_pred_label)

    predword_train = [ list(map(lambda x: config.l_in[x], y)) for y in train_pred_label]
    #print(predword_train)

    eval_result(labels, predword_train)
    
    print("Validating")
    acc, p, r, f1 = dev_and_test_char(val_data, val_labels, model, test = False)
    result.write('train lr' + str(float(K.get_value(model.optimizer.lr))) + ' decay: ' + str(float(K.get_value(model.optimizer.decay))) + "\nValidating " + ' acc: ' + str(acc) + ' p: ' + str(p) + ' r: ' + str(r) + ' f1: ' + str(f1)+'\n')
    if f1 >= best_f1:
        best_f1 = f1
        no_improve = 0
        model.save_weights(config.model_weight_path)
        model.save(config.model_path)
        print("new best score!")
        result.write("new best score!\n")
    else:
        no_improve += 1
        print("no_improve: " + str(no_improve))
        if no_improve >= config.train_limit:
            print("early stopping")
            result.write("early stopping\n")
            break

    print("Testing")
    acc, p, r, f1 = dev_and_test_char(test_data, test_labels, model, test = True)
    result.write("Testing " + ' acc: ' + str(acc) + ' p: ' + str(p) + ' r: ' + str(r) + ' f1: ' + str(f1)+'\n')
    result.write('\n')
    
