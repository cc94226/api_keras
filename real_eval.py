import sys,os

def real_eval(acc, pred):
    acc = float(acc)
    pred = float (pred)
    corpus = 'np'
    if corpus == 'mpl':
        api_num = 336.0
        word = 12000.0
    elif corpus == 'pd':
        api_num = 347.0
        word = 8492.0
    elif corpus == 'np':
        api_num = 240.0
        word = 10656.0
    elif corpus == 'jfc':
        api_num = 272.0
        word = 8401.0

    tp = round(api_num * acc)
    fn = api_num - tp
    total_pred = round(tp / pred)
    fp = total_pred - tp
    tn = word - api_num - fp
    acc = ( tp + tn ) / word
    precision = tp/(tp + fp)
    recall = tp / (tp + fn)
    f1 = (2 * tp) / (2 * tp + fp + fn)
    print("acc: "+ str(acc*100) + " precision: " + str(precision * 100) + " recall: " + str(recall * 100) + " f1: " + str(f1 * 100))
    print(tp)
    print(fn)
    print(total_pred)

if __name__ == '__main__':
    real_eval(sys.argv[1], sys.argv[2])
